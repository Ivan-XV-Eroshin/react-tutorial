FROM node:12

COPY package.json /
RUN npm i
COPY src/server /src/server
COPY src/server.js /src

#RUN apt update && apt upgrade -y
ENTRYPOINT node /src/server.js
