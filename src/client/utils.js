import {API_SERVER} from "./const";
import {getComment} from "./actions";

export const handleRead = (pathname) =>
    fetch(`${API_SERVER}${pathname}`)
        .then(res => res.json())
        .then(res =>
            res.success ?
                window.store.dispatch(getComment({
                    content: res.results.rows,
                })) :
                Promise.reject('error due server request')
        )
        .catch(err => alert(err))

export const handleCreate = (value,pathname) =>
    fetch(`${API_SERVER}${pathname}`, {
        method: 'PUT',
        headers: {'Content-Type': 'application/json',},
        body: JSON.stringify(value)
    })
        .then(res => res.json())
        .then(res =>
            res.success ?
                handleRead('/comments/1') :
                Promise.reject('error due server request'))
        .catch(err => alert(err))