import React, {Component} from 'react';
import {browserHistory, IndexRoute, Route, Router} from "react-router";
import {locations} from "./locations";
import App, { Wrapper} from "./App";
import {Logo} from "./assets/Logo";

/**
 * Компонент, обеспечивающий смену компонентов при переходе по ссылкам
 */
class Routes extends Component {
    render() {
        return (
            <Router history={browserHistory}>
                <Route path='/' component={App}>
                    <IndexRoute component={Logo}/>
                    <Route path='' component={Wrapper}>
                        {
                            locations.map(location =>
                                <Route
                                    key={location.path}
                                    path={location.path}
                                    component={location.component}
                                />
                            )
                        }
                    </Route>
                </Route>
            </Router>
        );
    }
}

export default Routes;