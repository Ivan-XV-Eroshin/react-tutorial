import React from 'react';
import './style/App.scss';
import 'bootstrap/dist/css/bootstrap.min.css'
import Menu from "./assets/Menu";
import ToggleSwitch from "./assets/ToggleSwitch";
import {changeTheme} from "./actions";
import {connect} from "react-redux";
import {DARK_THEME, LIGHT_THEME} from "./const";
import CommentSection from "./assets/CommentSection";

/**
 * Главный компонент
 * @param props
 * @returns {*}
 * @constructor
 */
function App(props) {
    const {theme, toggleTheme} = props;
    return (
        <React.Fragment>
            {
                window.location.pathname === '/' ?
                    null :
                    <header>
                        <Menu/>
                        <span translate='no'>REACT</span>
                    </header>
            }
            {props.children}
            {
                window.location.pathname === '/' ?
                    null :
                    <footer className={theme === DARK_THEME ? 'theme__dark' : ''}>
                        <ToggleSwitch label="Свет на странице" defaultChecked={theme === LIGHT_THEME}
                                      onClick={() => toggleTheme(theme === DARK_THEME ?
                                          LIGHT_THEME : DARK_THEME)}/>
                        <div id='author'>
                            <p>
                                Copyright © 2020 Ерошин Иван
                            </p>
                            <p>
                                ibs.eroshin.ivan@yandex.ru
                            </p>
                                Отдельная благодарность за консультацию:
                                <ul>
                                    <li>Новосёловой Елене - советы по дизайну</li>
                                    <li>Бурыкину Александру - советы по архитектуре</li>
                                    <li>и всем друзьям, кто тестировал этот сайт</li>
                                </ul>
                        </div>
                    </footer>
            }
        </React.Fragment>
    );
}

/**
 * Тело главного компонента
 * @param props
 * @returns {*}
 * @constructor
 */
let WrapperComponent = props => {
    const {theme} = props;
    return (
        <div id='wrapper' className={theme === DARK_THEME ? 'theme__dark' : ''}>
            <div className='content'>
                {props.children}
                <CommentSection/>
            </div>
        </div>
    )
};

const mapDispatchToProps = dispatch => ({
    toggleTheme: theme => dispatch(changeTheme(theme))
});

const mapStateToProps = state => ({
    theme: state.theme.currentTheme
});

export const Wrapper = connect(mapStateToProps, null)(WrapperComponent);
export default connect(mapStateToProps, mapDispatchToProps)(App);
