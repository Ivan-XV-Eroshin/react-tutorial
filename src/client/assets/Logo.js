import {Button} from "react-bootstrap";
import {locations} from "../locations";
import {browserHistory} from "react-router";
import {changeLocation} from "../actions";
import {connect} from "react-redux";
import React from "react";

/**
 * Лендинговая страничка, рендерит атом, кнопку и надпись
 * @param redirect
 * @returns {*}
 * @constructor
 */
let LogoComponent = ({redirect}) => {
    let electrons = [];
    for (let i = 0; i < 30; i++) {
        electrons.push(<div className={`rotated-${i}`} key={i}>
            <div className={`electron-${i} resize-${i}`}/>
        </div>)
    }
    return (
        <div className='greeting'>
            <div className='caption'>Добро пожаловать в React</div>
            <div className='atom-container'>
                {electrons}
                <div className='core'/>
            </div>
            <Button variant='primary' size='lg' id='start-btn' onClick={() => {
                redirect(locations[0]);
                window.scrollTo(0, 0)
                browserHistory.push('/beforeStart')
            }}>
                Начать!
            </Button>
        </div>
    )
}
const mapDispatchToProps = (dispatch) => ({
    redirect: location => dispatch(changeLocation(location))
});

export const Logo = connect(null, mapDispatchToProps)(LogoComponent)