import React from 'react';
import {Button} from "react-bootstrap";
import {browserHistory} from "react-router";
import {locations} from "../locations";
import {changeLocation} from "../actions";
import {connect} from "react-redux";
import {ForwardIcon} from './icons/ForwardIcon'

/**
 * Навигационная  панель, состоит из двух кнопок: вперёд и назад
 * @param currentLocation
 * @param redirect
 * @returns {*}
 * @constructor
 */
let NavPanel = ({currentLocation, redirect}) => {

    let location = {
        backLocation: locations[locations.indexOf(currentLocation) - 1],
        forwardLocation: locations[locations.indexOf(currentLocation) + 1]
    };

    return (
        <div className="nav-panel">
            {
                location.backLocation &&
                <Button variant='primary' size='lg' className='btn__left' onClick={() => {
                    redirect(location.backLocation);
                    window.scrollTo(0, 0);
                    browserHistory.push(location.backLocation.path)
                }}>
                    <ForwardIcon className='icon icon__back'/>
                    <span>К предыдущему уроку</span>
                </Button>
            }
            {
                location.forwardLocation &&
                <Button variant='primary' size='lg' className='btn__right' onClick={() => {
                    redirect(location.forwardLocation);
                    window.scrollTo(0, 0)
                    browserHistory.push(location.forwardLocation.path)
                }}>
                    <span>К следyющему уроку</span>
                    <ForwardIcon className='icon icon__forward'/>
                </Button>
            }
        </div>
    );
}

const mapDispatchToProps = (dispatch) => ({
    redirect: location => dispatch(changeLocation(location))
});

const mapStateToProps = state => ({
    currentLocation: state.location.currentLocation
});

export default connect(mapStateToProps, mapDispatchToProps)(NavPanel);






