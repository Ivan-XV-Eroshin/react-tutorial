import React, {Component} from 'react';
import {connect} from "react-redux";

/**
 * Название статьи
 */
class ArticleTitle extends Component {
    render() {
        return (
            <div className='subtitle article__title'>
                {this.props.currentLocation.label}
            </div>
        );
    }
}

const mapStateToProps = state => (
    {
        currentLocation: state.location.currentLocation
    }
)

export default connect(mapStateToProps, null)(ArticleTitle);