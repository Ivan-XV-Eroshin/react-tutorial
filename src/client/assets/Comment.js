import React from "react";
import PersonIcon from '@material-ui/icons/Person';
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography";

const CommentComponent = ({comments = []}) => {
    const commentsList = comments && comments.map(({name, comment_text, commentID, comment_time}, index) =>
        <li style={{borderBottom: "solid gray 1px", padding:".5em 0"}} key={index}>
            <div style={{display: "flex"}}>
                <Avatar avatar={<PersonIcon/>}/>
                <PersonName name={name}/>
                <Typography style={{flexGrow:6}} color="textSecondary" align={"right"}>{comment_time}</Typography>
            </div>
            <CommentText text={comment_text}/>
        </li>
    )
    return (
        <div style={{display: "flex", flexDirection: "column"}}>
            {commentsList}
        </div>
    )
}

const CommentText = ({text}) =>
    <div style={{padding: "0 2em"}}>
        {text}
    </div>

const PersonName = ({name}) =>
    <div>
        {name}
    </div>

const Avatar = ({avatar}) =>
    <div style={{width: "2em"}}>
        {avatar}
    </div>


export const Comment = connect(state => {console.log(state); return {comments: state.comments.content}}, dispatch => ({}))(CommentComponent);
