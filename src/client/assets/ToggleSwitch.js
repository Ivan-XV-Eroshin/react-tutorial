import React from 'react';

/**
 * Переключатель
 * @param label
 * @param id
 * @param other
 * @returns {*}
 * @constructor
 */
const ToggleSwitch = ({label, id,...other}) =>
    <div className='toggle-switch'>
        {
            label &&
            <span className="label">
                {label}
            </span>
        }
        <label className="switch" htmlFor={`checkbox_${id? id: ''}`}>
            <input type="checkbox" id={`checkbox_${id? id: ''}`} {...other}/>
            <span className="slider round"/>
        </label>
    </div>

export default ToggleSwitch;