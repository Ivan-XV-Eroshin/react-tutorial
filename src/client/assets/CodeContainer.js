import React from 'react'
import {CODE_STYLE, SyntaxHighlighter, USED_LANGUAGES} from '../const'
import PropTypes from 'prop-types'


/**
 * Контейнер для кода
 * @param props
 * @returns {*}
 * @constructor
 */
const CodeContainer = (props) => {
    const {code, language, classStyle, showLineNumbers, customStyle} = props
    let preStyle = {...customStyle};

    //задаём отступ внутри, если это не сделано извне,
    //нужно для корректного отображения нумерации, костыль
    if (!preStyle.padding) {
        Object.assign(preStyle, {padding: '1em 1em 1em 3em', marginTop: '0'})
    }

    //в случае, если нужно встроить контейнер в текст, то просто
    //убираем отступы, раздувающие контейнер и делаем внутренние
    //отступы от текста, чтобы текст в контейнере выглядел естественно
    if (classStyle === 'inline') {
        Object.assign(preStyle, {padding: '0 0.5em', margin: '0'})
    }

    return (
        <div className={`code-container ${classStyle}`}>
            <SyntaxHighlighter language={language}
                               style={CODE_STYLE}
                               customStyle={preStyle}
                               showLineNumbers={showLineNumbers}
                               lineNumberContainerProps={{className: 'number-column'}}>
                {code}
            </SyntaxHighlighter>
        </div>
    )

}

CodeContainer.propTypes = {
    code: PropTypes.string.isRequired,
    classStyle: PropTypes.string,
    customStyle: PropTypes.object,
    language: PropTypes.oneOf(USED_LANGUAGES).isRequired,
    showLineNumbers: PropTypes.bool
}
CodeContainer.defaultProps = {
    classStyle: '',
    customStyle: {},
    showLineNumbers: false,
}
export default CodeContainer
