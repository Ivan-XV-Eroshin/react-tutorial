import React, {Component} from 'react';
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";

/**
 * Компонент, содержащий вкладки
 */
class TabContainer extends Component {
    /**
     * Парсинг и рендер вкладок
     * @returns {*}
     */
    render() {
        let {tabs} = this.props;
        let captions, components;
        if (tabs){
            captions = tabs.map((tab, index) => {
                    return <Tab key={index}>{tab.caption}</Tab>
                }
            )
            components = tabs.map((tab, index)=>{
                return <TabPanel key={index}>{tab.component}</TabPanel>
            })
        }

        return (
            <Tabs>
                <TabList>
                    {captions}
                </TabList>
                {components}
            </Tabs>
        );
    }
}

export default TabContainer;