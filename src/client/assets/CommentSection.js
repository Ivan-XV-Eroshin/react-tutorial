import React, {useEffect, useState} from 'react';
import {Comment} from "./Comment";
import {Field, reduxForm, reset} from "redux-form";
import Button from "react-bootstrap/Button";
import {handleCreate, handleRead} from "../utils";
import {connect} from "react-redux";

const SectionTitle = ({title}) =>
    <p className='section__title'>
        {title}
    </p>


const InputField = ({input, meta: {error, touched}}) =>
    <>
        <textarea rows={5} style={{borderColor: touched && error ? "red" : "inherit"}} {...input} />
        {
            touched && error &&
            <p style={{color: "red"}}>{error}</p>
        }
    </>

const requiredValidator = value => value || typeof value === 'number' ? undefined : 'Обязательное поле';

const InputComment = ({handleSubmit}) =>
    <form onSubmit={handleSubmit} style={{display: "flex", flexDirection: "column"}}>
        <Field component={InputField} name='comment_text' validate={requiredValidator}/>
        <Button variant={"primary"} size={"sm"} type={"submit"} className="btn__send" style={{
            color: "white",
            background: "#3e4655 ",
            border: "none",
            boxShadow: "0 0 3px 0 black",
        }}>
            Оставить комметарий
        </Button>
    </form>

const InputCommentForm = reduxForm({
    form: "inputCommentForm",
    onSubmitSuccess: (result, dispatch) =>
        dispatch(reset('inputCommentForm'))
})(InputComment)


const Comments = () => {
    return (
        <ul style={{listStyle: "none", padding: "unset"}}>
            <Comment/>
        </ul>)
}

const Loader = () =>
    <div className="lds-ring">
        <div/>
        <div/>
        <div/>
        <div/>
    </div>


const CommentSection = (props) => {
    const [loaded, setLoaded] = useState(false)
    console.log()
    useEffect(() => {
        !loaded && handleRead('/comments/1').then(() => setLoaded(true))
    }, [loaded])
    return (
        <div className="section">
            <SectionTitle title="Комментарии"/>
            <InputCommentForm onSubmit={val => handleCreate({...val, comment_time: new Date()}, '/comments/')}/>
            {!loaded ?
                <Loader/> :
                <Comments/>

            }
        </div>
    );
};

export default CommentSection