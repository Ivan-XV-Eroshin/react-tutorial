import React from 'react';
import Popup from "reactjs-popup";
import {locations} from "../locations";
import {IndexLink} from "react-router";
import {connect} from "react-redux";
import {changeLocation} from "../actions";


/**
 * Компонент меню
 * @param redirect
 * @returns {*}
 * @constructor
 */
let Menu = ({redirect}) => (
    <div>
        <Popup trigger={<p id='nav-menu'>&#9776;</p>}
               contentStyle={{width: "auto"}}
               arrow={true}
               arrowStyle={{left: "10%"}}
               mouseEnterDelay={1}
               mouseLeaveDelay={1}>
            <div className='dropdown_menu'>
                {
                    locations.map(location =>
                        <IndexLink onClick={e => {
                            redirect(location);
                            window.scrollTo(0, 0);
                            document.getElementsByClassName('popup-overlay')[0].click()
                        }} key={location.path} to={location.path} className='dropdown_menu-item'>
                            {location.label}

                        </IndexLink>
                    )
                }
            </div>
        </Popup>
    </div>
);

const mapDispatchToProps = (dispatch) => ({
    redirect: location => dispatch(changeLocation(location))
});


export default connect(null, mapDispatchToProps)(Menu);