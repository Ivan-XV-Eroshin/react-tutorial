import Composition from "./pages/Composition";
import StateAndProps from "./pages/StateAndProps";
import UsingComponents from "./pages/UsingComponents";
import JSXIntroduce from "./pages/JSXIntroduce";
import BeforeStart from "./pages/BeforeStart";
import Importing from "./pages/Importing";
import Specialization from "./pages/Specialization";
import ConditionalRendering from "./pages/ConditionalRendering";
import AdditionalPackages from "./pages/AdditionalPackages";
import InstallingPackages from "./pages/InstallingPackages";
import ReactRedux from "./pages/ReactRedux";
import ReduxForm from "./pages/ReduxForm";
import PropTypes from "./pages/PropTypes";
import Conclusion from "./pages/Conclusion";

/**
 * Список всех возможных Компонентов и путей к ним
 * @type {*[]}
 */
export const locations = [
    {
        path: '/beforeStart',
        label: 'Инструменты',
        component: BeforeStart,
    }, {
        path: '/jsx-introduce',
        label: 'Введение в jsx',
        component: JSXIntroduce,
    }, {
        path: '/importing',
        label: 'Импорт',
        component: Importing,
    }, {
        path: '/usingComponents',
        label: 'Использование компонентов',
        component: UsingComponents,
    }, {
        path: '/stateAndProps',
        label: 'Стейт и пропсы',
        component: StateAndProps,
    }, {
        path: '/composition',
        label: 'Композиция',
        component: Composition,
    }, {
        path: '/specialization',
        label: 'Специализация',
        component: Specialization
    }, {
        path: '/conditionalRendering',
        label: 'Условный рендеринг',
        component: ConditionalRendering
    }, {
        path: '/packages',
        label: 'Дополнительные пакеты',
        component: AdditionalPackages
    }, {
        path: '/installPackages',
        label: 'Установка пакетов. Практические советы',
        component: InstallingPackages
    }, {
        path: '/redux',
        label: 'Архитектура приложений',
        component: ReactRedux
    }, {
        path: '/form',
        label: 'Формы',
        component: ReduxForm
    }, {
        path: '/proptypes',
        label: 'Проверка типов',
        component: PropTypes
    }, {
        path: '/conclusion',
        label: 'Заключение',
        component: Conclusion
    }
]