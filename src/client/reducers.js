import {CHANGE_PATH, CHANGE_THEME, GET_COMMENTS} from "./actions";
import {locations} from "./locations";
import {DARK_THEME, LIGHT_THEME} from "./const";

const initialLocation = {
    currentLocation: locations.filter(location => location.path === window.location.pathname)[0]
}

/**
 * Редуктор действий, связанных с положением на странице
 * @param state
 * @param action
 * @returns {{currentLocation: *}|({currentLocation}&{currentLocation: (*|le.currentLocation|Function)})}
 */
export const locationReducer = (state = initialLocation, action) => {
    switch (action.type) {
        case CHANGE_PATH: {
            return {
                ...state,
                currentLocation: action.payload.currentLocation
            }
        }
        default:
            return state;
    }
}

/**
 * Инициализация темы
 * @returns {string}
 */
const currentTheme = () => {
    const now = new Date();
    if (now.getHours() < 22 && now.getHours() > 8)
        return LIGHT_THEME;
    else
        return DARK_THEME;
}
const initialTheme = {
    currentTheme: currentTheme()
}

/**
 * Редуктор действий, связанных с изменением темы приложения
 * @param state
 * @param action
 * @returns {{currentTheme: string}|({currentTheme}&{currentTheme: (string|*)})}
 */
export const themeReducer = (state = initialTheme, action) => {
    switch (action.type) {
        case CHANGE_THEME: {
            return {
                ...state,
                currentTheme: action.payload.currentTheme
            }
        }
        default:
            return state;
    }
}

export const commentsReducer = (state = {content: []}, action) => {
    switch (action.type) {
        case GET_COMMENTS: {
            return {
                ...state,
                ...action.payload.content
            }
        }
        default :
            return state
    }
}