export const CHANGE_PATH = 'CHANGE_PATH';
export const CHANGE_THEME = 'CHANGE_THEME';
export const GET_COMMENTS = 'GET_COMMENTS';
/**
 * Генератор действия изменения положения
 * @param location
 * @returns {{payload: {currentLocation: *}, type: string}}
 */
export const changeLocation = location => ({
    type: CHANGE_PATH,
    payload: {
        currentLocation: location
    }
})

/**
 * Генератор действия смены темы
 * @param theme
 * @returns {{payload: {currentTheme: *}, type: string}}
 */
export const changeTheme = theme => ({
    type: CHANGE_THEME,
    payload: {
        currentTheme: theme
    }
})

export const getComment = content => ({
    type: GET_COMMENTS,
    payload: {
        content
    }
})
