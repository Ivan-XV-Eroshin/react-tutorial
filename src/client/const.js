import * as styles from "react-syntax-highlighter/dist/cjs/styles/prism"
import {PrismAsync as Highlighter} from "react-syntax-highlighter"


//Подсветка синтаксиса
export const CODE_STYLE = styles.tomorrow
export const USED_LANGUAGES = Highlighter.supportedLanguages
export const SyntaxHighlighter = Highlighter


export const LIGHT_THEME = 'LIGHT_THEME'
export const DARK_THEME = 'DARK_THEME'

export const API_SERVER= 'http://localhost:3002'