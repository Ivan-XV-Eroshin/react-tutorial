import React, {Component} from 'react';
import ArticleTitle from "../assets/ArticleTitle";
import NavPanel from "../assets/NavPanel";
import CodeContainer from "../assets/CodeContainer";
import {Field, reduxForm} from "redux-form";

/**
 * Компонент, содержащий информацию о работе с формами
 */
class ReduxForm extends Component {
    /**
     * Конструктор, задание стейта
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            name: null
        }
    }

    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='redux-form'>
                <div className='page-content'>
                    <ArticleTitle/>
                    <p>
                        В этой статье речь пойдёт о том, каким образом можно создавать различные формы в реакте.
                    </p>
                    <p>
                        В предыдущем уроке речь заходила о том, что redux также может обрабатывать формы. Почему бы не
                        воспользоваться этой возможностью прямо сейчас? Но каким бы простым не казался подход к созданию
                        приложений с помощью этих технологий, необходимо знать, какими атрибутами обладает тег input в
                        новых и старых стандартах html. Для ознакомления с этими параметрами можно посетить <a
                        href="https://www.w3.org/TR/2010/WD-html5-20101019/the-input-element.html">спецификацию
                        HTML5</a>. А тем временем мы приступаем к созданию простой формы, которая будет приветствовать
                        пользователя.
                    </p>
                    <p>
                        Давайте спроектируем процесс заполнения формы. Когда пользователь заходит на сайт, то ему
                        предлагается ввести имя и нажать кнопку подтвердить. После нажатия кнопки форма исчезает, а на
                        её месте рендерится текст, приветствующий пользователя по имени.
                    </p>
                    <p>
                        Предлагаемая реализация: в компоненте App прибегнуть к условному рендерингу (в случае, если не
                        существует свойства name, отрисовывать форму, в противном случае - приветственное сообщение).
                        При подтверждении формы выполнить простую валидацию имени (имя должно состоять только из букв).
                        Затем данные из формы отправить в стор с помощью действия с типом SEND_NAME (см. типы и
                        генераторы действий в предыдущем уроке).
                    </p>
                    <p>
                        Итак, приступим. Сперва нужно создать необходимый редьюсер, тип действия, генератор действия(см.
                        прердыдущий урок), затем подключить всё в index.js. Вот здесь возникает некоторое отличие. Дело
                        в том, что у redux-form есть собственный редьюсер, который обрабатывает формы, а у нас в
                        приложении есть свой, который обрабатывает изменение имени. Не беда, эти два редьюсера можно
                        объединить функцией combineReducers() таким образом:
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        'import ReactDOM from \'react-dom\';\n' +
                        'import \'./index.css\';\n' +
                        'import {Provider} from "react-redux";\n' +
                        'import {combineReducers, createStore} from "redux";\n' +
                        'import {reducer as formReducer} from \'redux-form\'\n' +
                        'import App from "./App";\n' +
                        'import {nameReducer} from "./reducers";\n' +
                        '//функция собирает все редьюсеры в один большой редьюсер\n' +
                        'const reducers = combineReducers({\n' +
                        '    user: nameReducer,\n' +
                        '    form: formReducer\n' +
                        '})\n' +
                        'const store = createStore(reducers);\n' +
                        '\n' +
                        'ReactDOM.render(\n' +
                        '    <Provider store={store}\x3e\n' +
                        '        <App/\x3e\n' +
                        '    </Provider\x3e\n' +
                        '    , document.getElementById(\'root\'));'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Затем создадим компонент для ввода текста на основе <span
                        translate="no">input type='text'</span>.
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        '\n' +
                        '//здесь выполнена декомпозиция объекта props на объекты\n' +
                        '//  meta и input, которые передаются компоненту из Field\n' +
                        '//  meta содержит в себе информацию о поле, было ли оно тронуто, была ли ошибка в поле и прочее\n' +
                        '//  input содержит в себе действия, которые обрабатываются редьюсером redux-form и некотороые другие поля\n' +
                        'const InputComponentReduxForm = ({meta, input}) =\x3e {\n' +
                        '    const hasIncorrectValue = meta.touched && (meta.error || meta.warning)\n' +
                        '    return (\n' +
                        '        /*здесь общий компонент для ввода*/\n' +
                        '        <div className=\'input input__box\'\x3e\n' +
                        '            {/*непосредственно поле ввода*/}\n' +
                        '            <input {...input} type=\'text\' className={hasIncorrectValue ? \'field__error\' : \'\'}/\x3e\n' +
                        '            {\n' +
                        '                /*а тут рендерится сообщение об ошибке*/\n' +
                        '                hasIncorrectValue &&\n' +
                        '                <div className="input__error"\x3e\n' +
                        '                    {meta.error}\n' +
                        '                    {meta.warning}\n' +
                        '                </div\x3e\n' +
                        '            }\n' +
                        '        </div\x3e\n' +
                        '    );\n' +
                        '};\n' +
                        '\n' +
                        'export default InputComponentReduxForm;'
                    } language='jsx' showLineNumbers={true}/>
                    <p className="note">
                        Внимание, здесь использовался синтаксис ES6 при передаче параметров в функцию (<a
                        href="https://learn.javascript.ru/destructuring-assignment#umnye-parametry-funktsiy">см умные
                        параметры функций</a>).
                    </p>
                    <p>
                        После этого напишем форму, которая будет использовать этот компонент.
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        'import {Field, reduxForm} from "redux-form";\n' +
                        'import InputComponentReduxForm from "./InputComponentReduxForm"\n' +
                        '//валидаторы, которые выдают строку с ошибкой при нарушении валидации\n' +
                        'const nameValidator = name => name && !/[A-Я]([а-я]{1,30})/g.test(name) ? undefined : \'Некорректное имя\'\n' +
                        'const requiredValidator = value =\x3e value || typeof value === \'number\' ? undefined : \'Обязательное поле\'\n' +
                        '\n' +
                        'const NameForm = (props) =\x3e {\n' +
                        '    return (\n' +
                        '        /*в props.handleSubmit содержится функция, которая собирает все значения с формы при её подтверждении и передаёт их наверх*/\n' +
                        '        <form className=\'form-name\' onSubmit={props.handleSubmit}\x3e\n' +
                        '            {/*в компонент Field передаются те поля, значения из которых нужно получить*/}\n' +
                        '            <Field component={InputComponentReduxForm} name=\'userName\' validate={[nameValidator, requiredValidator]}/\x3e\n' +
                        '            {/*А здесь кнопка отправки формы*/}\n' +
                        '            <button className=\'btn btn-primary\'\x3eОтправить</button\x3e\n' +
                        '        </form\x3e\n' +
                        '    );\n' +
                        '};\n' +
                        '\n' +
                        '//здесь опишем, с каким именем нужно создавать форму\n' +
                        'const createForm = reduxForm({form: \'NameForm\'});\n' +
                        '//и в конце создадим форму из созданного компонента\n' +
                        'export default createForm(NameForm);\n'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        И финальным штрихом будет реализация компонента App
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        'import {connect} from "react-redux";\n' +
                        'import NameForm from "./NameForm"\n' +
                        'import {sendNameAction} from \'./actions\'\n' +
                        '\n' +
                        'const App = props => {\n' +
                        '    return (\n' +
                        '        <>\n' +
                        '            {\n' +
                        '                /*если имя существует, то рендерить приветствие*/\n' +
                        '                props.name ?\n' +
                        '                    <h3>{props.name}, добро пожаловать!</h3> :\n' +
                        '                    /*если в компонент имя не передали, то рендерить форму\n' +
                        '                    * а в форме нужно передать как минимум один проп - onSubmit, который говорит,\n' +
                        '                    * что нужно делать с значениями из формы\n' +
                        '                    * Конкретно здесь используется анонимная стрелочная функция, в которой происходит\n' +
                        '                    * вызов функции sendName()\n' +
                        '                    * А функция sendName() в свою очередь создаётся mapDispatchToProps*/\n' +
                        '                    <NameForm onSubmit={values => props.sendName(values.userName)}/>\n' +
                        '            }\n' +
                        '        </>\n' +
                        '    );\n' +
                        '};\n' +
                        '\n' +
                        '/*стрелочная функция, возвращает объект, поля которого передаются\n' +
                        '* в props у того компонента, к которому он применен.\n' +
                        '* Эта функция нужна, чтобы у стейта приложения взять только часть, которая\n' +
                        '* нужна текущему компоненту.*/\n' +
                        'const mapStateToProps = state => {\n' +
                        '    return {\n' +
                        '        name: state.user.name\n' +
                        '    }\n' +
                        '}\n' +
                        '/*стрелочная функция, возвращает объект, поля которого передаются\n' +
                        '* в props у того компонента, к которому он применен.\n' +
                        '* Эта функция нужна, чтобы отправлять в стейт действия*/\n' +
                        'const mapDispatchToProps = dispatch => {\n' +
                        '    return {\n' +
                        '        /*здесь создаётся функция*/\n' +
                        '        sendName: function (name) {\n' +
                        '            //здесь создаём действие с помощью генератора действий\n' +
                        '            const createdAction = sendNameAction(name)\n' +
                        '            //а здесь отправляем это действие в стор\n' +
                        '            return dispatch(createdAction)\n' +
                        '        }\n' +
                        '    }\n' +
                        '}\n' +
                        '/*И в завершение подключаем с помощью функции connect к нашему компоненту\n' +
                        '* две вышесозданные функции, чтобы мы могли обращаться к полям \n' +
                        '* name, sendName у объекта props в компоненте App*/\n' +
                        'export default connect(mapStateToProps, mapDispatchToProps)(App);'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Ну вот теперь можно посмотреть на результат проделанной работы, он должен быть примерно
                        следующим.
                    </p>
                    {
                        this.state.name ? <h1>{this.state.name}, добро пожаловать!</h1> :
                            <NameForm onSubmit={values => this.setState({name: values.userName})}/>
                    }
                    <p>
                        Более подробную информацию о создании форм, валидации полей и обработке можете получить в <a
                        href="https://redux-form.com/">документации от разработчиков библиотеки redux-form.</a>
                    </p>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

//здесь выполнена декомпозиция объекта props на объекты
//  meta и input, которые передаются компоненту из Field
//  meta содержит в себе информацию о поле, было ли оно тронуто, была ли ошибка в поле и прочее
//  input содержит в себе действия, которые обрабатываются редьюсером redux-form и некотороые другие поля
const InputComponentReduxForm = ({meta, input}) => {
    const hasIncorrectValue = meta.touched && (meta.error || meta.warning)
    return (
        /*здесь общий компонент для ввода*/
        <div className='input input__box'>
            {/*непосредственно поле ввода*/}
            <input {...input} type='text' className={hasIncorrectValue ? 'field__error' : ''}/>
            {
                /*а тут рендерится сообщение об ошибке*/
                hasIncorrectValue &&
                <div className="input__error">
                    {meta.error}
                    {meta.warning}
                </div>
            }
        </div>
    );
};


const nameValidator = name => name && /[A-Я]([а-я]{1,30})/g.test(name) ? undefined : 'Некорректное имя'
const requiredValidator = value => value || typeof value === 'number' ? undefined : 'Обязательное поле'

let NameForm = (props) => {
    return (
        /*в props.handleSubmit содержится функция, которая собирает все значения с формы при её подтверждении и передаёт их наверх*/
        <form className='form-name' onSubmit={props.handleSubmit}>
            {/*в компонент Field передаются те поля, значения из которых нужно получить*/}
            <Field component={InputComponentReduxForm} name='userName' validate={[requiredValidator, nameValidator]}/>
            {/*А здесь кнопка отправки формы*/}
            <button style={{marginLeft: '1.5em'}} className='btn btn-primary'>Отправить</button>
        </form>
    );
};

NameForm = reduxForm({form: 'NameForm'})(NameForm)

export default ReduxForm;