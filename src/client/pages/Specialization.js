import React, {Component} from 'react';
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";
import CodeContainer from "../assets/CodeContainer";
import TabContainer from "../assets/TabContainer";

/**
 * Компонент, содержащий информацию о специализации
 */
class Specialization extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        let specTabs = [{
            caption: 'Application.js',
            component: <CodeContainer showLineNumbers={true}  code={
                'import React, {Component} from \'react\';\n' +
                'import ApplicationContainer from \'./ApplicationContainer\'\n' +
                'import FullContent from \'./FullContent\'\n' +
                'import DemoContent from \'./DemoContent\'\n' +
                'import FullMenu from \'./FullMenu\'\n' +
                'import DemoMenu from \'./DemoMenu\'\n' +
                '\n' +
                'class Application extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                <ApplicationContainer menu={this.props.isDemo ? <DemoMenu/\x3e : <FullMenu/\x3e}\n' +
                '                                      content={this.props.isDemo ? <DemoContent/\x3e : <FullContent/\x3e}/\x3e\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default Application;'
            } language='jsx'/>,
        }, {
            caption: 'ApplicationContainer.js',
            component: <CodeContainer showLineNumbers={true}  code={
                'import React, {Component} from \'react\';\n' +
                '\n' +
                'class ApplicationContainer extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <>\n' +
                '                <div className="menu">\n' +
                '                    {this.props.menu}\n' +
                '                </div>\n' +
                '                <div className="content">\n' +
                '                    {this.props.content}\n' +
                '                </div>\n' +
                '            </>\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default ApplicationContainer;'
            } language='jsx'/>,
        }, {
            caption: 'DemoMenu.js',
            component: <CodeContainer showLineNumbers={true}  code={
                'import React, {Component} from \'react\';\n' +
                '\n' +
                'class DemoMenu extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                {/*Реализация демо-меню, содержащая основные функции*/}\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default DemoMenu;'} language='jsx'/>,
        }, {
            caption: 'FullMenu.js',
            component: <CodeContainer showLineNumbers={true}  code={
                'import React, {Component} from \'react\';\n' +
                '\n' +
                'class FullMenu extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                {/*Реализация полного кода меню, содержащая все функции*/}\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default FullMenu;'
            } language='jsx'/>,
        }, {
            caption: 'DemoContent.js',
            component: <CodeContainer showLineNumbers={true}  code={
                'import React, {Component} from \'react\';\n' +
                '\n' +
                'class DemoContent extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                {/*Демонстрация части контента, направленной на привлечение пользователя*/}\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default DemoContent;'
            } language='jsx'/>,
        }, {
            caption: 'FullContent.js',
            component: <CodeContainer showLineNumbers={true}  code={
                '\n' +
                'import React, {Component} from \'react\';\n' +
                '\n' +
                'class FullContent extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                {/*Демонстрация всего контента страницы*/}\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default FullContent;'
            } language='jsx'/>,
        },
        ]
        return (
            <div className='specialization'>
                <div className='page-content'>

                    <ArticleTitle/>
                    <p>
                        Для того, чтобы повысить масштабируемость приложения, создаются некоторые контнейнеры и
                        обобщения, которые принимают в себя различные компоненты.
                    </p>
                    <p>
                        Хорошим примером может быть две версии приложения: демо-версия и полная версия. В зависимости от
                        того, какой статус у аккаунта, можно часть панелей не отображать или рендерить прощённые
                        компоненты.
                    </p>

                    <p>

                    </p>
                    <TabContainer tabs={specTabs}/>
                    <p className="note">
                        В приведенном примере компоненты разбросаны по файлам, что является одним из правил хорошего
                        тона. Выделение модулей из кода позволяет их переиспользвать в дальнейшем внутри этого же
                        проекта или в других проектах, тем самым уменьшая время реализации. Высвобожденное время можно
                        потратить на более тщательную проработку архитектуры приложения или на реализацию нового
                        функционала.
                    </p>
                    <p>
                        В компоненте ApplicationContainer размечены области для меню и содержимого с помощью блоков,
                        однако, структура компонента может быть намного сложнее. В этот контейнер передаются компоненты,
                        которые реализуют разный функционал. Для передачи используется тернарный оператор, суть которого
                        заключается в том, если выполняется условие, то будет передано значение до двоеточия, в
                        противном случае будет передано то, что после него.
                    </p>
                    <p>
                        Этот оператор используется при условном рендеринге, о котором речь пойдет в следующем уроке.
                    </p>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default Specialization;