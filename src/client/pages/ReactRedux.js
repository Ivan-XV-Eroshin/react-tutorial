import React, {Component} from 'react';
import ArticleTitle from "../assets/ArticleTitle";
import NavPanel from "../assets/NavPanel";
import CodeContainer from "../assets/CodeContainer";

/**
 * Компонент, содержащий информацию об архитектуре
 */
class ReactRedux extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (

            <div className='react-redux'>
                <div className='page-content'>
                    <ArticleTitle/>
                    <p>
                        Что же такое <span translate="no">redux</span>, в чём его смысл? Этот пакет позволит приложению
                        на <span translate="no">React</span> получить большую гибкость. Суть всего пакета сводится к 7
                        строкам, если опустить проверку типов и прочую валидацию.
                    </p>
                    <CodeContainer code={
                        'function createStore(reducer, initialState) {\n' +
                        '    let state = initialState\n' +
                        '    return {\n' +
                        '        dispatch: action => { state = reducer(state, action) },\n' +
                        '        getState: () => state,\n' +
                        '    }\n' +
                        '}'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Redux является реализацией Flux-архитектуры — паттерна для организации передачи данных в
                        React-приложениях.В классическом Flux для хранения стейта приложения используется стор.
                        Диспатчинг (передача) экшенов вызывает изменение этого стейта. После этого происходит перерендер
                        представления в соответствии с измененным стейтом. Flux упрощает разработку, создавая
                        однонаправленный поток данных. Это уменьшает спагетти-эффект по мере роста кодовой базы
                        приложения. Одна из сложностей в понимании работы Redux — это множество неочевидных терминов
                        типа редюсеров, селекторов и санков.
                    </p>
                    <p>
                        Здесь же мы рассмотрим только необходимый набор для работы всего приложения.
                    </p>
                    <p>
                        Первое, что нужно создать, это тип действия. Как правило, он описывает, что происходит в
                        приложении. Выглядит это как простая экспортируемая строка. Как правило, тип действия помещается
                        в файл с названием actionTypes.js.
                    </p>
                    <CodeContainer code={'export const CHANGE_PATH = \'CHANGE_PATH\';'} language={'jsx'}
                                   showLineNumbers={true}/>
                    <p>
                        Следующим небольшим шагом является создание генератора действия. На текущий момент описано, что
                        будет происходит в приложении (изменяться положение), однако, нужно предоставить дополнительную
                        информацию о том, что происходит (куда происходит перенаправление). Именно для этого пишутся
                        генераторы действий, которые помещаются в файл с названием actions.js
                    </p>
                    <CodeContainer code={
                        'import CHANGE_PATH from \'./actionTypes\'\n' +
                        '//функция просто принимает информацию о действии и создает объект, который описывает действие\n' +
                        'export function changeLocation(location) {\n' +
                        '    //создадим объект, который содержит информацию о действии\n' +
                        '    const action = {\n' +
                        '        type: CHANGE_PATH,//тип конкретного действия\n' +
                        '        payload: {//и содержимое, которое нужно отправить в стор\n' +
                        '            currentLocation: location\n' +
                        '        }\n' +
                        '    };\n' +
                        '    return action;//а затем эта функция вернёт созданное действие\n' +
                        '}'
                    } language='jsx' showLineNumbers={true}/>
                    <p>
                        Далее надо создать обработчик действий (редьюсер). Обработчики охватывают ограниченное
                        количество действий. Так, например, за обработку изменений маршрута будет отвечать один
                        редьюсер, а за отображение модальных окон - другой, изменения в формах для ввода отслеживаются
                        третьим редьюсером, четвёртый редьюсер отвечает за взаимодействие с сервером. Но для работы
                        всего приложения достаточно и одного редьюсера, который сейчас будет создан. Обработчики
                        помещаются в файл с названием reducers.js.
                    </p>
                    <CodeContainer code={
                        'import {CHANGE_PATH} from "./actions";\n' +
                        '//функция-редьюсер принимает состояние приложения и действие\n' +
                        'export function locationReducer(state, action){\n' +
                        '    switch (action.type) {//смотрим какой тип у действия\n' +
                        '        case CHANGE_PATH: {//и записываем в поле в состоянии, которое отвечает за текущее местоположене\n' +
                        '            return {\n' +
                        '                ...state,//оставляем всё состояние как оно было, но дальше изменим в нём одно свойство\n' +
                        '                //мы знаем, что в поле payload.currentLocation действие содержит\n' +
                        '                // информацию о текущем положении\n' +
                        '                currentLocation: action.payload.currentLocation\n' +
                        '            }\n' +
                        '        }\n' +
                        '        default:\n' +
                        '            //если же тип действия не попадает в "зону ответственности" конкретного редьюсера, то оставляем стейт как он был\n' +
                        '            return state;\n' +
                        '    }\n' +
                        '}'
                    } language={'jsx'} showLineNumbers={true}/>
                    <div className='note'>
                        Для написания этого редьюсера использовался синтаксис ES6. Конструкция
                        <CodeContainer code={'...state'} language='jsx' classStyle='inline'/> дословно обозначает взять
                        все поля у объекта state, а затем скопировать их в новый объект.
                    </div>
                    <p>
                        И последним штрихом для того, чтобы вся эта конструкция заработала, является создание состояния,
                        которое будет хранить информацию о том, что сейчас происходит в приложении. Выполняется это всё
                        достаточно просто. В файле index.js, который был создан при генерации приложения, нужно внести
                        следующие изменения:
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        'import ReactDOM from \'react-dom\';\n' +
                        'import \'./index.css\';\n' +
                        'import * as serviceWorker from \'./serviceWorker\';\n' +
                        'import {Provider} from "react-redux";\n' +
                        'import {createStore} from "redux";\n' +
                        'import App from "./App";\n' +
                        'import {locationReducer} from "./reducers";\n' +
                        '//создать стор с помощью функции createStore, в которую передать редьюсер\n' +
                        'const store = createStore(locationReducer);\n' +
                        '//и обернуть компонент App компонентом Provider\n' +
                        'ReactDOM.render(\n' +
                        '    <Provider store={store}\x3e\n' +
                        '        <App/\x3e\n' +
                        '    </Provider\x3e\n' +
                        '    , document.getElementById(\'root\'));\n' +
                        '\n' +
                        'serviceWorker.unregister();'
                    } language='jsx' showLineNumbers={true}/>

                    <p>
                        Более подробную информацию по redux можно найти <a href="https://redux.js.org/">в официальной
                        документации</a>, <a
                        href="https://habr.com/ru/post/439104/">в этой статье на хабре</a> <a
                        href="https://medium.com/devschacht/redux-step-by-step-e6c42a9b00cd">и в статье на медиуме</a>
                    </p>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default ReactRedux;