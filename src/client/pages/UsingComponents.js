import React, {Component} from 'react';
import CodeContainer from "../assets/CodeContainer";
import TabContainer from "../assets/TabContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, содержащий информацию о использовании компонентов
 */
class UsingComponents extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        let filesTabs = [{
            caption: 'Greeting.js',
            component: <CodeContainer code={'import React, {Component} from \'react\';\n' +
            '\n' +
            'export class Greeting extends Component {\n' +
            '    render() {\n' +
            '        return (\n' +
            '            <h1\x3e\n' +
            '                Привет, мир!\n' +
            '            </h1\x3e\n' +
            '        );\n' +
            '    }\n' +
            '}'} language='jsx' showLineNumbers={true}/>
        }, {
            caption: 'App.js',
            component: <CodeContainer code={
                'import React, {Component} from \'react\';\n' +
                'import {Greeting} from \'./Greeting\';\n' +
                '\n' +
                'class App extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                <Greeting/\x3e\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default App;'
            } language='jsx' showLineNumbers={true}/>
        }];
        let importingTabs = [{
            caption: `<>`,
            component: <CodeContainer code={'import Greeting from "./components/Greeting"'} language='jsx'/>
        }, {
            caption: 'Полный код',
            component: <CodeContainer code={'import React, {Component} from \'react\';\n' +
            'import {Greeting} from \'./components/Greeting\';\n' +
            '\n' +
            'class App extends Component {\n' +
            '    render() {\n' +
            '        return (\n' +
            '            <div\x3e\n' +
            '                <Greeting/\x3e\n' +
            '            </div\x3e\n' +
            '        );\n' +
            '    }\n' +
            '}\n' +
            '\n' +
            'export default App;'} language='jsx' showLineNumbers={true}/>
        }];
        return (
            <div className='using-components'>
                <div className='page-content'>

                    <ArticleTitle/>

                    <p>
                        В этом уроке мы применим знания, полученные из двух предыдущих уроков
                    </p>
                    <p>
                        Давайте создадим первое приложение, которое выводит сообщение "Привет, мир!"
                        большими буквами. Для него нам потребуется тег h1, а также новый компонент,
                        потому что App является вершиной иерархии всех компонентов, итак App.js сейчас
                        содержит следующий код
                    </p>

                    <CodeContainer code={
                        'import React, {Component} from \'react\';\n' +
                        '\n' +
                        'class App extends Component {\n' +
                        '    render() {\n' +
                        '        return (\n' +
                        '            <div\x3e\n' +
                        '                <h1\x3e Привет, мир!</h1\x3e\n' +
                        '            </div\x3e\n' +
                        '        );\n' +
                        '    }\n' +
                        '}\n' +
                        '\n' +
                        'export default App;'
                    } language='jsx' showLineNumbers={true}/>
                    <p>
                        Вроде не сложно, но с добавлением новых фраз у нас будет разрастаться код одного файла,
                        связи будут становиться намного сложнее, дебаг такого кода со временем превратится в очень
                        сложный процесс, поэтому предлагается следующий рефакторинг: приветствие вынести в отдельный
                        компонент,
                        который потом подключить к нашему приложению.
                    </p>
                    <TabContainer tabs={filesTabs}/>
                    <div>
                        Итак, рассмотрим, что же за незнакомый тег
                        <CodeContainer code={'<Greegting/\x3e'} language={'jsx'} classStyle='inline'/>
                        появился в нашем коде. А это всего лишь тот компонент, который был создан в файле
                        Greeting.js.
                        <div className='note'>
                            Старайтесь давать файлам понятные имена, потому что проект разрастается, и через
                            несколько месяцев активной разработки очень сложно найти нужный файл или компонент.
                            А также хорошим тоном является разбиение содержимого файла по нескольким, так
                            как это поможет переиспользовать ваш код крайне эффективно.
                            Выделение компонентов в дальнейшем также упрощает процесс отладки, делает наглядным
                            дебаг, к тому же <span translate='no'>React</span> выводит, в каком компоненте и файле произошёл сбой
                        </div>
                    </div>
                    <p>
                        Обратите внимание, что название компонентов всегда пишутся с большой буквы,
                        это позволяет <span translate='no'>React</span> отделить пользовательские теги от HTML-тегов, которые пишутся полностью
                        маленькими буквами.
                    </p>
                    <div className='note'>
                        Все файлы, которые создаются, должны лежать в директории src, где <span translate='no'>React</span> создает файлы App.js и
                        index.js,
                        или же в дочерних директориях, иначе при поиске файла <span translate='no'>React</span> просто выдаст ошибку. Например, наш
                        файл можно поместить по следующему пути %путь до папки с проектом%/src/components. То есть мы
                        создали
                        папку components в папке src, затем переместили файл Greeting.js. Чтобы ничего не сломалось,
                        надо добавить
                        <TabContainer tabs={importingTabs}/>
                    </div>
                    <p>
                        А теперь можно поэкспериментировать с названиями компонентов, расположением
                        их в директории и импортом из других мест.
                    </p>
                    <NavPanel/>

                </div>
            </div>
        );
    }
}

export default UsingComponents;
