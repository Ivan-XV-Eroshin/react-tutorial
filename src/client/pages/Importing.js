import React, {Component} from 'react';
import CodeContainer from "../assets/CodeContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, содержащий описание импорта
 */
class Importing extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='importing'>
                <div className='page-content'>

                    <ArticleTitle/>
                    <p>
                        Первое с чего должен начинаться любой .js или .jsx файл в нашем приложении, если мы хотим
                        использовать React,
                        это особая строка, которая подключает <span translate='no'>React</span>.
                    </p>
                    <CodeContainer code={'import React from "React"'} language='jsx' showLineNumbers={true}/>
                    <p>
                        Здесь используется особое ключевое слово import, о котором пойдет речь в данном уроке.
                    </p>
                    <p>
                        Это слово позволяет подключить и использовать переменные из других файлов, что повышает
                        мобильность приложения, независимость составляющих частей друг от друга.
                        Также можно использовать классы и функции, которые по сути тоже являются переменными,
                        они лишь указывают на область памяти, в которой лежит код для выполнения.
                        Вдобавок, можно создавать особые конфигурационные файлы, в которых находятся
                        глобальные константы для ваших приложений или отдельных модулей,
                        которые можно потом просто подключить этим ключевым словом.
                    </p>
                    <p>
                        Чтобы сделать любой параметр импортируемым, нужно перед ним написать ключевое слово
                        export. Далее будет приведен пример использования этих ключевых слов.
                    </p>
                    <p>
                        File1.js
                    </p>
                    <CodeContainer code={
                        'export const constantValue = 5;\n' +
                        'export function customFunction(param) {\n' +
                        '    return param*param\n' +
                        '}\n' +
                        'export class MyClass{\n' +
                        '    method(param){\n' +
                        '        return param+param\n' +
                        '    };\n' +
                        '}'
                    } language='jsx' showLineNumbers={true}/>
                    <p>
                        File2.js
                    </p>
                    <CodeContainer code={
                        'import {constantValue, customFunction, MyClass} from "../File1";\n' +
                        'var instance = new MyClass();\n' +
                        'instance.method(constantValue);    //returns 25\n' +
                        'customFunction(constantValue);     //returns 10'
                    } language='jsx' showLineNumbers={true}/>
                    <p>
                        Давайте разбираться, что тут происходит
                    </p>
                    <div>
                        В первом файле у нас определена константа constantValue, функция
                        customFunction и класс, содержащий метод внутри себя. Перед их объявлением стоит
                        ключевое слово
                        <CodeContainer code={'export'} language={'jsx'} classStyle={'inline'}/>, которое
                        предоставляет доступ к ним извне.
                    </div>
                    <div>
                        Второй файл же содержит "волшебную" строку
                        <CodeContainer code={
                            'import {/*что нужно взять*/} ' +
                            'from "/*откуда это следует это брать*/"'
                        } language='jsx'/>
                    </div>
                    <div className='note'>
                        В примерах ранее после экспорта было ещё одно "волшебное" слово
                        <CodeContainer code={'default'} language='jsx' classStyle={'inline'}/>, которое
                        вносит особенность поведения - экспорт по умолчанию. В таком случае в строке с словом
                        import то, что следует экспортировать, не следует заключать в фигурные скобки.
                        Экспортироваться по умолчанию может только один класс, функция или константа
                    </div>
                    <p>
                        А сейчас просто поиграйте с этой замечательной возможностью, почувствуйте всё мощь
                        этой фишки языка. В следующем уроке мы совместим свежие знания с опытом из предыдущего урока
                        и научимся создавать и использовать свои собственные теги.
                    </p>


                    <NavPanel/>
                </div>

            </div>
        );
    }
}

export default Importing;