import React, {Component} from 'react';
import ArticleTitle from "../assets/ArticleTitle";
import NavPanel from "../assets/NavPanel";
import CodeContainer from "../assets/CodeContainer";
import TabContainer from "../assets/TabContainer";

/**
 * Компонент, содержащий информацию об установке пакетов
 */
class InstallingPackages extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        const packageContent = [{
            caption: "До",
            component: <CodeContainer code={
                '{\n' +
                '  "name": "package-name",\n' +
                '  "version": "0.1.0",\n' +
                '  "private": true,\n' +
                '  "dependencies": {\n' +
                '    "@testing-library/jest-dom": "^4.2.4",\n' +
                '    "@testing-library/react": "^9.4.0",\n' +
                '    "@testing-library/user-event": "^7.2.1",\n' +
                '    "node-sass": "^4.13.1",\n' +
                '    "prop-types": "latest",\n' +
                '    "react": "^16.12.0",\n' +
                '    "react-dom": "^16.12.0",\n' +
                '    "react-redux": "^7.2.0",\n' +
                '    "react-scripts": "3.4.0",\n' +
                '    "redux": "^4.0.5",\n' +
                '    "redux-form": "^8.3.0",\n' +
                '    "redux-logger": "^3.0.6"\n' +
                '  },\n' +
                '  "scripts": {\n' +
                '    "start": "react-scripts start",\n' +
                '    "build": "react-scripts build",\n' +
                '    "test": "react-scripts test",\n' +
                '    "eject": "react-scripts eject"\n' +
                '  },\n' +
                '  "eslintConfig": {\n' +
                '    "extends": "react-app"\n' +
                '  },\n' +
                '  "browserslist": {\n' +
                '    "production": [\n' +
                '      ">0.2%",\n' +
                '      "not dead",\n' +
                '      "not op_mini all"\n' +
                '    ],\n' +
                '    "development": [\n' +
                '      "last 1 chrome version",\n' +
                '      "last 1 firefox version",\n' +
                '      "last 1 safari version"\n' +
                '    ]\n' +
                '  }\n' +
                '}'
            } language='json' showLineNumbers={true}/>
        }, {
            caption: "После",
            component: <CodeContainer code={
                '{\n' +
                '  "name": "package-name",\n' +
                '  "version": "0.1.0",\n' +
                '  "private": true,\n' +
                '  "dependencies": {\n' +
                '    "@testing-library/jest-dom": "^4.2.4",\n' +
                '    "@testing-library/react": "^9.4.0",\n' +
                '    "@testing-library/user-event": "^7.2.1",\n' +
                '    "node-sass": "^4.13.1",\n' +
                '    "prop-types": "latest",\n' +
                '    "react": "^16.12.0",\n' +
                '    "react-dom": "^16.12.0",\n' +
                '    "react-redux": "^7.2.0",\n' +
                '    "react-router": "^3.2.5",\n' +
                '    "react-router-dom": "^5.1.2",\n' +
                '    "react-scripts": "3.4.0",\n' +
                '    "redux": "^4.0.5",\n' +
                '    "redux-form": "^8.3.0",\n' +
                '    "redux-logger": "^3.0.6"\n' +
                '  },\n' +
                '  "scripts": {\n' +
                '    "start": "react-scripts start",\n' +
                '    "build": "react-scripts build",\n' +
                '    "test": "react-scripts test",\n' +
                '    "eject": "react-scripts eject"\n' +
                '  },\n' +
                '  "eslintConfig": {\n' +
                '    "extends": "react-app"\n' +
                '  },\n' +
                '  "browserslist": {\n' +
                '    "production": [\n' +
                '      ">0.2%",\n' +
                '      "not dead",\n' +
                '      "not op_mini all"\n' +
                '    ],\n' +
                '    "development": [\n' +
                '      "last 1 chrome version",\n' +
                '      "last 1 firefox version",\n' +
                '      "last 1 safari version"\n' +
                '    ]\n' +
                '  }\n' +
                '}'
            } language={'json'} showLineNumbers={true}/>
        }]
        return (
            <div className='installingPackages'>
                <div className='page-content'>
                    <ArticleTitle/>
                    <p>
                        В этом уроке будет рассмотрен процесс установки пакетов на практике с общими сведениями о
                        пакете.
                    </p>

                    <p>
                        Во-первых, давайте установим в наше приложение <span translate='no'>redux</span> и все пакеты,
                        которые позволяют ему работать с реактом. Установку произведём стандартным способом через
                        командную строку.
                    </p>
                    <CodeContainer code={
                        'npm i redux\n' +
                        'npm i react-redux'
                    } language='bash'/>
                    <p>
                        Выполнив эти две команды, в проект будет добавлена библиотека <span translate='no'>Redux</span>.
                        Она реализует <span translate="no">Flux-like</span> архитектуру, позволяющую организовать
                        максимально прозрачный поток данных в приложении. Данный подход решает массу проблем с
                        сохранением состояния приложения, улучшает масштабируемость приложений и упрощает процесс
                        сопровождения приложения.
                    </p>
                    <p>
                        Следующим пакетом будет <span translate="no">redux-form</span>, который позволит создавать и
                        гибко настраивать формы на основе всё того же <span translate="no">redux</span>. Устанавливать
                        будем последнюю версию этого пакета.
                    </p>
                    <CodeContainer code={'npm i redux-form@latest'} language='bash'/>
                    <p className='note'>
                        Слово @latest в конце указывает на то, что пакетному менеджеру нужно взять последнюю версию
                        этого пакета (см предыдущий урок).
                    </p>
                    <p>
                        Библиотека <span translate="no">redux-form</span> позволяет создавать формы, обработка которых
                        происходит через ранее установленный <span translate="no">redux</span>. Сбор всех данных с формы
                        происходит автоматически при подтверждении данных введённых в форму.
                    </p>
                    <p>
                        Также давайте установим ещё пакет с графическими элементами (кнопки, сетки, вкладки, элементы
                        меню, разного рода подсказки, аккордеоны, карусели, модальные окна, хлебные крошки). Одним из
                        таких решений является <span translate="no">react-bootstrap</span>. Устанавливается всё очень
                        просто - одной командой в директории проекта.
                    </p>
                    <CodeContainer code='npm i react-bootstrap bootstrap' language='bash'/>
                    <p>
                        Далее нам будет необходим пакет для организации навигации по приложению. Для добавления этого
                        функционала воспользуемся файлом package.json, в котором хранится вся информация о текущем
                        пакете. Давайте найдём в этом файле запись "dependencies"
                    </p>
                    <TabContainer tabs={packageContent}/>
                    <p>
                        На 14 и 15 строке указаны пакеты, которые необходимо добавить в файл package.json. После того,
                        как файл package.json будет сохранён, следует выполнить в директории проекта команду
                    </p>
                    <CodeContainer code={'npm i'} language={'bash'}/>
                    <p>
                        Эта команда установит те пакеты, которые не были установлены, но присутствуют в файле
                        package.json
                    </p>
                    <p>
                        У всех установленных пакетов можно найти документацию непосредственно в интернете, но в
                        следующих уроках будут рассмотрены основные моменты из некоторых пакетов, которые облегчат
                        дальнейший поиск информации по этим пакетам.
                    </p>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default InstallingPackages;