import React, {Component} from 'react';
import ArticleTitle from "../assets/ArticleTitle";
import NavPanel from "../assets/NavPanel";
import CodeContainer from "../assets/CodeContainer";

/**
 * Компонент, содержащий описание условного рендера
 */
class ConditionalRendering extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='conditionalRendering'>
                <div className='page-content'>
                    <ArticleTitle/>

                    <p>
                        В ряде случаев требуется рендерить внутри компонентов содержимое в зависимости от состояния.
                    </p>

                    <p>
                        Рассмотрим два компонента:
                    </p>
                    <CodeContainer showLineNumbers={true} code={
                        'function UserGreeting(props) {\n' +
                        '  return <h1\x3eС возвращением!</h1\x3e;\n' +
                        '}\n' +
                        '\n' +
                        'function GuestGreeting(props) {\n' +
                        '  return <h1\x3eВойдите, пожалуйста.</h1\x3e;\n' +
                        '}'
                    } language='jsx'/>

                    <p>
                        Можно создать компонент Greeting, который отражает один из этих компонентов в зависимости от
                        того, на сайте пользователь или нет:
                    </p>
                    <CodeContainer showLineNumbers={true} code={
                        'function Greeting(props) {\n' +
                        '  const isLoggedIn = props.isLoggedIn;\n' +
                        '  if (isLoggedIn) {\n' +
                        '    return <UserGreeting /\x3e;\n' +
                        '  }\n' +
                        '  return <GuestGreeting /\x3e;\n' +
                        '}\n' +
                        '\n' +
                        'ReactDOM.render(\n' +
                        '  // Попробуйте заменить на isLoggedIn={true} и посмотрите на эффект.\n' +
                        '  <Greeting isLoggedIn={false} /\x3e,\n' +
                        '  document.getElementById(\'root\')\n' +
                        ');'
                    } language='jsx'/>
                    <p>
                        В этом примере рендерится различное приветствие в зависимости от значения пропа isLoggedIn.
                    </p>
                    <p>
                        Однако такой подход усложняет читабельность кода, так как есть несколько точек выхода и
                        возможных сценариев отрисовки. Для улучшения читабельности кода можно воспользоваться тернарным
                        оператором или логическим оператором "и". Давайте перепишем созданный компонент с использованием
                        этих элементов.
                    </p>
                    <CodeContainer showLineNumbers={true} code={
                        'const Greeting = props =\x3e {\n' +
                        '    const isLoggedIn = props.isLoggedIn;\n' +
                        '    return (\n' +
                        '        <div\x3e\n' +
                        '            {isLoggedIn && <UserGreeting/\x3e}\n' +
                        '            {isLoggedIn ? null : <GuestGreeting/\x3e}\n' +
                        '        </div\x3e\n' +
                        '    );\n' +
                        '}\n' +
                        '\n' +
                        'ReactDOM.render(\n' +
                        '    // Попробуйте заменить на isLoggedIn={true} и посмотрите на эффект.\n' +
                        '    <Greeting isLoggedIn={false}/\x3e,\n' +
                        '    document.getElementById(\'root\')\n' +
                        ');'
                    } language='jsx'/>
                    <div className="note">
                        Для того, чтобы встраивать javascript-выражения в jsx, используются фигурные скобочки. Когда
                        интерпретатор доходит до них, ему поступает чёткая инструкция взять значение (в нашем случае
                        компонент) и отрендерить.
                    </div>
                    <p>
                        На 5 строчке в примере представлено использование логического && оператора "и".
                        А на 6 строке можно остановиться подробнее. Мы рассмотрели вариант, когда переменная isLoggedIn
                        может быть равна true. Тогда в 5 строке отрендерится приветствие пользователя, значит в 6
                        строчке нам не нужно ничего рендерить, чтобы не создавать дублей компонентов. И для этого вторым
                        параметром у тернарного оператора является null. В случае, если значение переменной isLoggedIn
                        false, 5 строка не вернёт компонент, а 6 вернёт. Однако, 5 и 6 строчку можно объединить
                    </p>
                    <CodeContainer code={'const Greeting = ({isLoggedIn}) => (\n' +
                    '    <div\x3e\n' +
                    '        {isLoggedIn ? <UserGreeting/\x3e : <GuestGreeting/\x3e}\n' +
                    '    </div\x3e\n' +
                    ')\n' +
                    '\n' +
                    'ReactDOM.render(\n' +
                    '    // Попробуйте заменить на isLoggedIn={true} и посмотрите на эффект.\n' +
                    '    <Greeting isLoggedIn={false}/\x3e,\n' +
                    '    document.getElementById(\'root\')\n' +
                    ');'} language='jsx' showLineNumbers={true}/>
                    <div className='note'>
                        В этом примере очень много "синтаксического сахара" - синтаксических конструкций языка, которые
                        позволяют сократить запись программы. Во-первых, здесь используется <a
                        href='https://learn.javascript.ru/arrow-functions-basics'>стрелочная функция</a>.
                        Во-вторых, к аргументу props применяется <a
                        href='https://learn.javascript.ru/destructuring-assignment#umnye-parametry-funktsiy'>
                        деструктуризация объекта</a> и внутрь функции передаётся один параметр,
                        <CodeContainer code={'({isLoggedIn})'} language={'jsx'} showLineNumbers={false}
                                       classStyle='inline'/> а не весь объект props. И завершает всю эту изящную
                        конструкцию <a href='https://riptutorial.com/javascript/example/17665/implicit-return'>неявный
                        возврат значения</a> (неявный return). И вместо 9 строк кода на компонент получилось 5 без
                        потери функционала, но с большей концентрацией информации на строку, что облегчает процесс
                        отладки.
                    </div>
                    <p>
                        На этом основной функционал реакта не заканчивается, но с его помощью уже можно писать элементы
                        приложения. Однако, для ускорения процесса разработки потребуется использовать дополнительные
                        пакеты, о чем речь пойдет в следующем уроке.
                    </p>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default ConditionalRendering;

