import React from 'react'
import CodeContainer from "../assets/CodeContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, содержащий введение
 */
class BeforeStart extends React.Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='before-start'>
                <div className='page-content'>
                    <ArticleTitle/>
                    <p>
                        Прежде чем начинать разработку на <span translate='no'>React</span>нужно разобраться со средой
                        разработки. Код можно писать прямо в браузере, например <a href='https://codepen.io/gaearon/pen/oWWQNa?editors=0010'>Здесь.</a> Это
                        пример игры крестики-нолики, предоставляемый разработчиками реакта.
                        Но такой метод разработки подойдёт только для знакомства, для углублённого изучения нужно
                        настраивать локальное окружение, о чем речь пойдет далее:
                    </p>
                    <ul>
                        <li>
                            <p className='subtitle'>
                                Node.js
                            </p>
                            <div>
                                Для начала надо установить программную платформу, которая позволяет взаимодействовать
                                с js с официального
                                сайта <a href='https://nodejs.org/en/'>Скачать можно отсюда.</a> В установленной
                                платформе нас будет интересовать в основном один инструмент - Node Package Manager
                                или npm. Менеджер пакетов Node.js, который позволит отслеживать и использовать все
                                установленные
                                пакеты библиотек. Проверить установку Node можно простой командой
                                <CodeContainer code={'npm -v'} language={'batch'}/>
                                Ожидаемый вывод примерно следующий, версия установленного npm:
                                <CodeContainer code={'6.13.4'} language={'jsx'}/>
                                Если вывода не последовало, то просто перезагрузите компьютер.
                            </div>
                            <p/>
                        </li>
                        <li>
                            <p className='subtitle'>
                                Среда разработки
                            </p>
                            <p>
                                Следующим шагом будет установка IDE, предоставляющей возможности
                                по редактированию javascript-кода.
                                Для создания этого проекта использовалась среда разработки Intellij
                                IDEA, которая предоставляет мощный функционал по написанию кода на js.
                                Также можно использовать такие инструменты, как Eclipse, Visual Studio.
                                Использование инструментального ПО необходимо, так как оно значительно
                                ускоряет процесс написания кода и выполняет тривиальные задачи
                            </p>
                        </li>
                        <li>
                            <p className="subtitle">
                                Средства отладки
                            </p>
                            <p>
                                Отлаживать код можно в большинстве популярных браузеров. Хром предоставляет
                                мощный набор инструментов для отладки кода, который носит название
                                Chrome DevTools, устанавливается в большинстве случаев вместе с браузером
                                по умолчанию, однако, можно произвести и отдельную установку.
                                У Mozilla и Opera имеются схожие наборы инструментов. Подробнее можно прочитать
                                <a href="https://habr.com/ru/company/simbirsoft/blog/337116/">{' тут'}</a>.
                                Также потребуется специальный инструмент для отладки именно <span
                                translate='no'>React</span>-компонентов,
                                который называется <span translate='no'>React</span> Developer Tools, устанавливается в
                                виде отдельного
                                расширения. Версию для хрома можно скачать
                                <a href="https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi">
                                    {' в магазине расширений Chrome'}
                                </a>.
                            </p>
                        </li>
                        <li>
                            <p className='subtitle'>
                                Создание каркаса приложения
                            </p>
                            <p>
                                Следующим шагом будет использования <span translate='no'>React</span> с помощью команды
                            </p>
                            <CodeContainer code={'npx create-react-app my-app\n' +
                            'cd my-app\n' +
                            'npm start'}
                                           language={'batch'}/>
                            <p>Готово! Теперь можно приступить к созданию своего первого приложения
                                на React.
                            </p>
                            <div className='note'>
                                <p>Примечание</p>

                                <CodeContainer code={'npx'} language='batch' classStyle='inline'/>
                                в первой строке не является опечаткой.
                                Это инструмент запуска пакетов, доступный в версиях npm 5.2 и выше.

                            </div>

                        </li>
                    </ul>
                    <p>
                        Итак, мы познакомились с тем, как создать приложение на <span translate='no'>React</span>,
                        теперь давайте посмотрим, как
                        писать код в App.js файле
                    </p>

                    <NavPanel/>
                </div>
            </div>
        );
    }

}

export default BeforeStart