import React, {Component} from 'react';
import CodeContainer from "../assets/CodeContainer";
import TabContainer from "../assets/TabContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, содержащий информацию о состоянии и свойствах
 */
class StateAndProps extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        let filesTabs = [{
            caption: 'Greeting.js',
            component: <CodeContainer code={
                'import React from \'react\';\n' +
                '\n' +
                'export function Greeting (props) {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <h1\x3e\n' +
                '                Добро пожаловать, {props.userName}!\n' +
                '            </h1\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}'} language='jsx' showLineNumbers={true}/>
        }, {
            caption: 'App.js',
            component: <CodeContainer code={
                'import React, {Component} from \'react\';\n' +
                'import {Greeting} from \'./Greeting\';\n' +
                '\n' +
                'class App extends Component {\n' +
                '    render() {\n' +
                '        return (\n' +
                '            <div\x3e\n' +
                '                <Greeting userName="Михаил"/\x3e\n' +
                '            </div\x3e\n' +
                '        );\n' +
                '    }\n' +
                '}\n' +
                '\n' +
                'export default App;'
            } language='jsx' showLineNumbers={true}/>
        }];
        return (

            <div className='using-components'>
                <div className='page-content'>

                    <ArticleTitle/>

                    <p>
                        Хорошо, компоненты научились создавать, но все они обладают низким уровнем
                        абстракции, так как рендерят одно и то же, где бы их не вызвали.
                        В реакте есть специальные объекты, которые исправляют этот недостаток.
                        И имя этим объектам state и props, как вы уже догадались, именно о них
                        пойдет речь в этом уроке.
                    </p>
                    <p>
                        Для чего нужен state, а для чего - props?
                    </p>
                    <p>
                        Для ответа на этот вопрос придется немного освежить в памяти предыдущий урок,
                        где мы создавали свои компоненты. Там было одно очень важное замечание по поводу
                        компонентов - их названия должны начинаться с заглавной буквы, это было введено
                        неспроста. При использовании собственного компонента все атрибуты <span translate='no'>React</span> упаковывает
                        значения всех переданных атрибутов в объект - props.
                    </p>
                    <p>
                        Итак, props - это ни что иное, как набор параметров, передаваемый для создаваемого
                        компонента, некоторый фиксированный набор входных значений, который не должен меняться
                        со временем.
                    </p>
                    <p>
                        Вот пример использования этого свойства в компонентах из предыдущего урока.
                    </p>
                    <TabContainer tabs={filesTabs}/>
                    <div>
                        Когда мы делаем вызов компонента таким образом:
                        <CodeContainer code={'<Greeting userName="name"/\x3e'} language='jsx' classStyle='inline'/>
                        То <span translate='no'>React</span> самостоятельно собирает значение всех атрибутов в один объект - props
                    </div>
                    <div>
                        В формальной записи это будет выглядеть следующим образом:
                        <CodeContainer code={
                            "<Some attribute1='value1' attribute2='value2' attributeN='valueN'/\x3e"
                        } language='jsx' showLineNumbers={true}/>
                        Такой вызов приводит к тому, что компоненту MyComponent передается следующий объект props:
                        <CodeContainer code={
                            'props = {\n' +
                            '    attribute1: \'value1\',\n' +
                            '    attribute2: \'valule2\',\n' +
                            '    attributeN: \'valueN\'\n' +
                            '}'
                        } language='jsx' showLineNumbers={true}/>
                        <div className="note">
                            Как вы могли заметить, здесь все значения являются строковыми, чтобы
                            передать значение или объект, его следует поместить в фигурные скобочки,
                            например, таким образом:
                            <CodeContainer code={
                                '<Greeting userName="Михаил" showName={false}/\x3e'
                            } language='jsx' classStyle={'inline'}/>
                        </div>
                        <div className="note">
                            Важное замечание.
                            <div>
                                Все компоненты должны вести себя по отношению к пропсам, как
                                <a href="https://habr.com/ru/post/437512/">{' ЧИСТЫЕ ФУНКЦИИ '}</a>.
                            </div>
                        </div>
                    </div>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default StateAndProps;