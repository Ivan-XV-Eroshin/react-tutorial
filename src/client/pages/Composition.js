import React, {Component} from 'react';
import CodeContainer from "../assets/CodeContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, описывающий композицию
 */
class Composition extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='importing'>
                <div className='page-content'>

                    <ArticleTitle/>
                    <p> Хорошо, в прошлом уроке мы научились использовать компоненты внутри других компонентов. А что
                        делать, если компонент не знает, что ему рендерить, если контент зависит, например, от выбора
                        пользователя? Это особенно характерно для таких компонентов, которые представляют из себя как бы
                        контейнер или коробку, в которую можно что-то положить. Как пример, аватарка в социальной сети,
                        она
                        может быть как в круглой рамке, так и в квадратной. Для таких компонентов рекомендуется
                        использовать
                        специальный проп children, который передаст дочерние элементы сразу на вывод:
                    </p>
                    <CodeContainer code={
                        'import React, {Component} from \'react\';\n' +
                        '\n' +
                        'class ImageContainer extends Component {\n' +
                        '    render() {\n' +
                        '        return (\n' +
                        '            <div\x3e\n' +
                        '                {this.props.children}\n' +
                        '            </div\x3e\n' +
                        '        );\n' +
                        '    }\n' +
                        '}\n' +
                        '\n' +
                        'export default ImageContainer;'
                    } language={'jsx'} showLineNumbers={true}/>
                    <div className='note'>
                        Так, а здесь что-то новенькое: встретилось выражение
                        <CodeContainer code={'{this.props.children}'}
                                       language={'jsx'}
                                       classStyle={'inline'}/>
                        Что же оно обозначает? Оказывается, jsx позволяет отображать переменные внутри html, для этого
                        достаточно обернуть эту переменную в фигурные скобки. Хорошо, что же тогда за переменная такая
                        this.props.children? Ответ на этот вопрос прост - наш компонент расширяет Component, в котором
                        как раз-таки и содержится это свойство.
                    </div>
                    <p>
                        Это позволит передать компоненту произвольные дочерние элементы, вложив их в JSX:
                    </p>
                    <CodeContainer code={
                        'import React, {Component} from \'react\';\n' +
                        'import ImageContainer from "./ImageContainer"\n' +
                        'import SquareImg from "./SquareImg"\n' +
                        '\n' +
                        'class ProfileImage extends Component {\n' +
                        '    render() {\n' +
                        '        return (\n' +
                        '            <div\x3e\n' +
                        '                <ImageContainer\x3e\n' +
                        '                    <SquareImg/\x3e\n' +
                        '                </ImageContainer\x3e\n' +
                        '            </div\x3e\n' +
                        '        );\n' +
                        '    }\n' +
                        '}\n' +
                        '\n' +
                        'export default ProfileImage;'
                    } language={'jsx'} showLineNumbers={true}/>

                    <p>
                        Здесь в компонент ImageContainer вкладывается квадратное изображение, хотя это не обязательно.
                    </p>
                    <p>
                        Всё, что находится внутри JSX-тега передаётся в компонент ImageContainer через проп
                        children. Поскольку ImageContainer рендерит внутри children, все переданные элементы
                        отображаются в конечном выводе.
                    </p>
                    <p>
                        В проп children можно передавать любое значение. Однако, не стоит злоупотреблять этим свойством,
                        так как оно значительно снижает читабельность кода. Чтобы избежать большинства проблем
                        используют специализацию, о которой речь пойдет в следующем уроке.
                    </p>


                    <NavPanel/>
                </div>
            </div>

        );
    }
}

export default Composition;

