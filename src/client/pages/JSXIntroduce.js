import React from 'react';
import CodeContainer from "../assets/CodeContainer";
import NavPanel from "../assets/NavPanel";
import ArticleTitle from "../assets/ArticleTitle";

/**
 * Компонент, содержащий информацию о синтаксисе JSX
 */
class JSXIntroduce extends React.Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='jsx-intro'>
                <div className='page-content'>

                    <ArticleTitle/>

                    Перед нами содержимое App.js файла
                    <CodeContainer code={"import React from 'react';\n" +
                    "import logo from './logo.svg';\n" +
                    "import './App.css';\n" +
                    "\n" +
                    "function App() {\n" +
                    "  return (\n" +
                    "    <div className=\"App\"\x3E\n" +
                    "      <header className=\"App-header\"\x3E\n" +
                    "        <img src={logo} className=\"App-logo\" alt=\"logo\" /\x3E\n" +
                    "        <p\x3E\n" +
                    "          Edit <code\x3Esrc/App.js</code\x3E and save to reload.\n" +
                    "        </p\x3E\n" +
                    "        <a\n" +
                    "          className=\"App-link\"\n" +
                    "          href=\"https://reactjs.org\"\n" +
                    "          target=\"_blank\"\n" +
                    "          rel=\"noopener noreferrer\"\n" +
                    "        \x3E\n" +
                    "          Learn React\n" +
                    "        </a\x3E\n" +
                    "      </header\x3E\n" +
                    "    </div\x3E\n" +
                    "  );\n" +
                    "}\n" +
                    "\n" +
                    "export default App;"} language={'jsx'} showLineNumbers={true}/>

                    <p>
                        В нём есть привычные нам HTML-теги, но они обёрнуты в какой-то странный javascript-код.
                        Давайте разбираться в чем тут дело.
                        Всё объясняется особой философией и синтаксисом <span translate='no'>React</span>, который позволяет создавать
                        свои собственные теги, но обо всём по порядку
                    </p>
                    <p>
                        Как же всё рендерится в HTML-код? <span translate='no'>React</span> предлагает подход к созданию своих собственных
                        компонентов: функциональных или классовых. В приведенном примере показан функциональный
                        компонент
                        App, который можно использовать вне файла благодаря ключевому слову export. Если изучить
                        подробнее
                        созданный проект, можно
                        увидеть файл index.js, в котором и используется этот компонент:
                    </p>
                    <CodeContainer code={'import React from \'react\';\n' +
                    'import ReactDOM from \'react-dom\';\n' +
                    'import \'./index.css\';\n' +
                    'import App from \'./App\';\n' +
                    'import * as serviceWorker from \'./serviceWorker\';\n' +
                    '\n' +
                    'ReactDOM.render(<App /\x3E, document.getElementById(\'root\'));\n' +
                    '\n' +
                    'serviceWorker.unregister();\n'} language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Особый метод <span translate='no'>ReactDOM.render</span> рендерит компонент App на веб-странице.
                        Этот или другой любой другой компонент должен содержать в себе все компоненты, которые
                        необходимо отображать
                        на страничке, должен являться вершиной иерархии компонентов исходя из правил хорошего кода.
                    </p>
                    <p>
                        Создаются эти компоненты очень просто
                        Для классового компонента.
                    </p>
                    <CodeContainer code={
                        'export default JSXIntroduce;\n' +
                        '\n' +
                        'import React, {Component} from \'react\';\n' +
                        '\n' +
                        'class MyComponent extends Component {\n' +
                        '    render() {\n' +
                        '        return (\n' +
                        '            <div\x3E\n' +
                        '                \n' +
                        '            </div\x3E\n' +
                        '        );\n' +
                        '    }\n' +
                        '}\n' +
                        '\n' +
                        'export default MyComponent;'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Функциональный компонент выглядит примерно следующим образом:
                    </p>
                    <CodeContainer code={
                        'import React from \'react\';\n' +
                        '\n' +
                        '\n' +
                        'export const MyComponent = (props) => {\n' +
                        '    return (\n' +
                        '        <div\x3e\n' +
                        '\n' +
                        '        </div\x3e\n' +
                        '    );\n' +
                        '};'
                    } language={'jsx'} showLineNumbers={true}/>
                    <div className='note'>
                        Внимание, здесь используется стрелочная функция, введена недавно, эта запись эквивалентна
                        <CodeContainer code={'function MyComponent(props){\n' +
                        '//some code here\n' +
                        '}\n' +
                        'export MyComponent;'} language={'jsx'} showLineNumbers={true}/>
                    </div>
                    <p>Выглядит непонятно, правда? Какой-то франкенштейн из js и html тегов, на самом деле
                        нам пока что достаточно писать свой код внутри div компонентов, которые будут отрендерены
                        браузером, можете попробовать поиграть с html тегами внутри компнентов, в следующем
                        уроке разберёмся, как использовать созданные элементы
                    </p>


                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default JSXIntroduce

