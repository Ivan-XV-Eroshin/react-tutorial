import React, {Component} from 'react';
import 'react-bootstrap/Button/'
import ArticleTitle from "../assets/ArticleTitle";
import NavPanel from "../assets/NavPanel";
import CodeContainer from "../assets/CodeContainer";

/**
 * Компонент, содержащий информацию о библиотеки работы с типами
 */
class PropTypes extends Component {
    /**
     * Рендер контента
     * @returns {*}
     */
    render() {
        return (
            <div className='proptypes'>
                <div className='page-content'>
                    <ArticleTitle/>
                    <p>
                        В этой статье мы коснёмся безопасности кода, который пишется.
                    </p>
                    <p>
                        По мере роста вашего приложения вы можете отловить много ошибок с помощью проверки типов. Для
                        этого можно использовать расширения JavaScript вроде Flow и TypeScript. Но, даже если вы ими не
                        пользуетесь, React предоставляет встроенные возможности для проверки типов. Для запуска этой
                        проверки на пропсах компонента вам нужно использовать специальное свойство propTypes:
                    </p>
                    <CodeContainer code={
                        'import PropTypes from \'prop-types\';\n' +
                        '\n' +
                        'class Greeting extends React.Component {\n' +
                        '  render() {\n' +
                        '    return (\n' +
                        '      <h1\x3eПривет, {this.props.name}</h1\x3e\n' +
                        '    );\n' +
                        '  }\n' +
                        '}\n' +
                        '\n' +
                        'Greeting.propTypes = {\n' +
                        '  name: PropTypes.string\n' +
                        '};'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        PropTypes предоставляет ряд валидаторов, которые могут использоваться для проверки, что
                        получаемые данные корректны. В примере мы использовали PropTypes.string. Когда какой-то проп
                        имеет некорректное значение, в консоли будет выведено предупреждение. По соображениям
                        производительности propTypes проверяются только в режиме разработки.
                    </p>
                    <p>
                        Список возможных валидаторов приведен ниже
                    </p>
                    <CodeContainer code={
                        'import PropTypes from \'prop-types\';\n' +
                        '\n' +
                        'MyComponent.propTypes = {\n' +
                        '  // Можно объявить проп на соответствие определённому JS-типу.\n' +
                        '  // По умолчанию это не обязательно.\n' +
                        '  optionalArray: PropTypes.array,   //массив\n' +
                        '  optionalBool: PropTypes.bool,     //логический тип\n' +
                        '  optionalFunc: PropTypes.func,     //функция\n' +
                        '  optionalNumber: PropTypes.number, //число\n' +
                        '  optionalObject: PropTypes.object, //объект\n' +
                        '  optionalString: PropTypes.string, //строка\n' +
                        '  optionalSymbol: PropTypes.symbol, //Symbol\n' +
                        '\n' +
                        '  // Все, что может быть отрендерено:\n' +
                        '  // числа, строки, элементы или массивы\n' +
                        '  // (или фрагменты) содержащие эти типы\n' +
                        '  optionalNode: PropTypes.node,\n' +
                        '\n' +
                        '  // React-элемент\n' +
                        '  optionalElement: PropTypes.element,\n' +
                        '\n' +
                        '  // Тип React-элемент (например, MyComponent).\n' +
                        '  optionalElementType: PropTypes.elementType,\n' +
                        '  \n' +
                        '  // Можно указать, что проп должен быть экземпляром класса\n' +
                        '  // Для этого используется оператор `instanceof`.\n' +
                        '  optionalMessage: PropTypes.instanceOf(Message),\n' +
                        '\n' +
                        '  // Вы можете задать ограничение конкретными значениями\n' +
                        '  // при помощи перечисления\n' +
                        '  optionalEnum: PropTypes.oneOf([\'News\', \'Photos\']),\n' +
                        '\n' +
                        '  // Объект, одного из нескольких типов\n' +
                        '  optionalUnion: PropTypes.oneOfType([\n' +
                        '    PropTypes.string,\n' +
                        '    PropTypes.number,\n' +
                        '    PropTypes.instanceOf(Message)\n' +
                        '  ]),\n' +
                        '\n' +
                        '  // Массив объектов конкретного типа\n' +
                        '  optionalArrayOf: PropTypes.arrayOf(PropTypes.number),\n' +
                        '\n' +
                        '  // Объект со свойствами конкретного типа\n' +
                        '  optionalObjectOf: PropTypes.objectOf(PropTypes.number),\n' +
                        '\n' +
                        '  // Объект с определённой структурой\n' +
                        '  optionalObjectWithShape: PropTypes.shape({\n' +
                        '    color: PropTypes.string,\n' +
                        '    fontSize: PropTypes.number\n' +
                        '  }),\n' +
                        '  \n' +
                        '  // Объект со строгой структурой,\n' +
                        '  // при наличии необъявленных свойств будут сформированы предупреждения\n' +
                        '  optionalObjectWithStrictShape: PropTypes.exact({\n' +
                        '    name: PropTypes.string,\n' +
                        '    quantity: PropTypes.number\n' +
                        '  }),   \n' +
                        '\n' +
                        '  // Можно добавить`isRequired` к любому приведённому выше типу,\n' +
                        '  // чтобы показывать предупреждение,\n' +
                        '  // если проп не передан\n' +
                        '  requiredFunc: PropTypes.func.isRequired,\n' +
                        '\n' +
                        '  // Значение любого типа\n' +
                        '  requiredAny: PropTypes.any.isRequired,\n' +
                        '\n' +
                        '};'
                    } language={'jsx'} showLineNumbers={true}/>
                    <p>
                        Данный механизм также может быть полезен при создании утилит - универсальных компонентов,
                        которые можно переиспользовать в разных проектах; а также он необходим при создании библиотек из
                        компонентов. Проверка типов может помочь всем, кто будет использовать библиотеку, обойти большое
                        количество ошибок. Потому что при создании библиотеки только её разработчики знают о назначениях
                        параметров внутри компонентов, а с помощью propTypes они заявляют всем пользователям, что можно
                        передавать в компонент, а что нельзя.
                    </p>
                    <p>
                        Также можно задавать значения по умолчанию изящным способом - через defaultProps. Выглядит это
                        очень просто: определяете поле в пропсах и его значение, которое ему будет передано, даже если
                        родительский компонент не сделал этого.
                    </p>
                    <CodeContainer code={
                        'class Greeting extends React.Component {\n' +
                        '  render() {\n' +
                        '    return (\n' +
                        '      <h1\x3eПривет, {this.props.name}</h1\x3e\n' +
                        '    );\n' +
                        '  }\n' +
                        '}\n' +
                        '\n' +
                        '// Задание значений по умолчанию для пропсов:\n' +
                        'Greeting.defaultProps = {\n' +
                        '  name: \'Незнакомец\'\n' +
                        '};\n' +
                        '\n' +
                        '// Отрендерит "Привет, Незнакомец":\n' +
                        'ReactDOM.render(\n' +
                        '  <Greeting /\x3e,\n' +
                        '  document.getElementById(\'example\')\n' +
                        ');'
                    } language={'jsx'} showLineNumbers={true}/>
                    <NavPanel/>
                </div>
            </div>
        );
    }
}

export default PropTypes;