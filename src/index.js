import React from 'react';
import ReactDOM from 'react-dom';
import './client/index.css';
import * as serviceWorker from './client/serviceWorker';
import {Provider} from "react-redux";
import {applyMiddleware, combineReducers, createStore} from "redux";
import {reducer as formReducer} from 'redux-form'
import Routes from "./client/Routes";
import {commentsReducer, locationReducer, themeReducer} from "./client/reducers";
import {logger} from "redux-logger";

/**
 * Создание глобального хранилища
 * @type {Store<any, AnyAction> & Store<S & {}, A> & {dispatch: any}}
 */
const store = createStore(
    combineReducers({
        comments: commentsReducer,
        theme: themeReducer,
        location: locationReducer,
        form: formReducer
    }),
    applyMiddleware(logger)
);

window.store = store;

//рендер всего приложения
ReactDOM.render(
    <Provider store={store}>
        <Routes/>
    </Provider>
    , document.getElementById('root'));


serviceWorker.register();
