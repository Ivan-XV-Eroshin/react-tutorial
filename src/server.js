const cors = require('cors');

const express = require('express'),
    app = express(),
    port = process.env.PORT || 3002,
    bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

const routes = require('./server/routes/routes');
routes(app);

app.listen(port);

console.log('todo list RESTful API server started on: http://localhost:' + port);

const testConn = async () => {
    const {Client} = require('pg')
    const client = new Client({
        host: process.env.HOST || "localhost",
        port: process.env.PORT || "5432",
        user: "master_db",
        password: process.env.PASSWORD || "1234",
        database: process.env.DATABASE|| "react-learn",
    })
    await client.connect(err => {
        if (err) {
            console.error('connection error', err.stack)
        } else {
            console.log('connected')
        }
    })
    try {
        const res = await client.query('SELECT $1::text as message', ['Hello world!'])
        console.log(res.rows[0].message) // Hello world!
    } catch (e) {
        console.log(e)
    }
    await client.end()
}

testConn().then(()=>console.log("succeed")).catch(()=>console.log("error"))
