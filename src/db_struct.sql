--
-- PostgreSQL database dump
--

-- Dumped from database version 10.16 (Debian 10.16-1.pgdg90+1)
-- Dumped by pg_dump version 10.16 (Debian 10.16-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: articles; Type: TABLE; Schema: public; Owner: master_db
--

CREATE TABLE public.articles (
    article_id integer NOT NULL,
    article_title text NOT NULL,
    keywords text NOT NULL
);


ALTER TABLE public.articles OWNER TO master_db;

--
-- Name: articles_article_id_seq; Type: SEQUENCE; Schema: public; Owner: master_db
--

CREATE SEQUENCE public.articles_article_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.articles_article_id_seq OWNER TO master_db;

--
-- Name: articles_article_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: master_db
--

ALTER SEQUENCE public.articles_article_id_seq OWNED BY public.articles.article_id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: master_db
--

CREATE TABLE public.comments (
    comment_id integer NOT NULL,
    user_id integer NOT NULL,
    comment_text text NOT NULL,
    refer_comment integer,
    article_id integer NOT NULL,
    comment_time date NOT NULL
);


ALTER TABLE public.comments OWNER TO master_db;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: master_db
--

CREATE SEQUENCE public.comments_comment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comments_comment_id_seq OWNER TO master_db;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: master_db
--

ALTER SEQUENCE public.comments_comment_id_seq OWNED BY public.comments.comment_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: master_db
--

CREATE TABLE public.users (
    user_id integer NOT NULL,
    name character varying(100),
    salt character varying(20) NOT NULL,
    creation_date date,
    pass_hash character varying(100) NOT NULL
);


ALTER TABLE public.users OWNER TO master_db;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: master_db
--

CREATE SEQUENCE public.users_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_user_id_seq OWNER TO master_db;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: master_db
--

ALTER SEQUENCE public.users_user_id_seq OWNED BY public.users.user_id;


--
-- Name: articles article_id; Type: DEFAULT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.articles ALTER COLUMN article_id SET DEFAULT nextval('public.articles_article_id_seq'::regclass);


--
-- Name: comments comment_id; Type: DEFAULT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.comments ALTER COLUMN comment_id SET DEFAULT nextval('public.comments_comment_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.users ALTER COLUMN user_id SET DEFAULT nextval('public.users_user_id_seq'::regclass);


--
-- Data for Name: articles; Type: TABLE DATA; Schema: public; Owner: master_db
--

COPY public.articles (article_id, article_title, keywords) FROM stdin;
\.


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: master_db
--

COPY public.comments (comment_id, user_id, comment_text, refer_comment, article_id, comment_time) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: master_db
--

COPY public.users (user_id, name, salt, creation_date, pass_hash) FROM stdin;
\.


--
-- Name: articles_article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: master_db
--

SELECT pg_catalog.setval('public.articles_article_id_seq', 1, false);


--
-- Name: comments_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: master_db
--

SELECT pg_catalog.setval('public.comments_comment_id_seq', 1, false);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: master_db
--

SELECT pg_catalog.setval('public.users_user_id_seq', 1, false);


--
-- Name: articles articles_pk; Type: CONSTRAINT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.articles
    ADD CONSTRAINT articles_pk PRIMARY KEY (article_id);


--
-- Name: comments comments_pk; Type: CONSTRAINT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pk PRIMARY KEY (comment_id);


--
-- Name: users users_pk; Type: CONSTRAINT; Schema: public; Owner: master_db
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pk PRIMARY KEY (user_id);


--
-- PostgreSQL database dump complete
--
