const {query} = require("../connection");
const responseBuilder = (results, succeeded) => ({
    success: succeeded,
    results: {
        rows: results && results.rows
    }
})

exports.get = (req, res) => {
    console.log(req.params)
    query("SELECT a.article_id, comment_text, comment_time, name, comment_id FROM comments join articles a on comments.article_id = a.article_id join users u on comments.user_id = u.user_id where a.article_id = $1", [req.params.id])
        .then(results => {
            res.json(responseBuilder(results, true));
        })
        .catch(err => {
            //4. Если у нас что-то сломалось, то выводим в консоль на сервере ошибку
            console.error(err);
            console.log(`${err.name} code: ${err.code}`);
            //5. И в ответе говорим клиенту, что всё плохо.
            res.json({
                success: false,
                body: {
                    error: err.name
                }
            })
        });
}
exports.post = (req, res) => {
}
exports.put = async (req, res) => {
    console.log(req.body)
    query("insert into comments (user_id, comment_text, article_id, comment_time) values (1, $1, 1, $2);", req.body)
        .then(results => {
            //3.Отправляем их клиенту в формате .json
            res.json(responseBuilder(results, true));
        })
        .catch(err => {
                console.error(err);
                res.json({
                    success: false
                })
            }
        )
}

exports.delete = (req, res) => {
}