module.exports = app => {
    let commentController = require("../controllers/comment-controller")
    app.route('/comments/')
        .get((req, res) => res.json({success: true}))
        .put(commentController.put)
        .post(commentController.post)

    app.route('/comments/:id')
        .get(commentController.get)
        .delete(commentController.delete)
}