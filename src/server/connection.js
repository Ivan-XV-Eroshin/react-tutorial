
'use strict';

//получение библиотечного объекта, который содержит в себе все соединения
const {Pool} = require("pg");

//создание пула с настройками
const connectionsPool = new Pool({
    host: process.env.HOST || "localhost",
    port: process.env.PORT || "5432",
    user:"master_db",
    password: process.env.PASSWORD || "1234",
    database: process.env.DATABASE|| "react-learn",
    max: 20,
    connectionTimeoutMillis: 0,
    idleTimeoutMillis: 0
});

exports.pool = connectionsPool;

//создание функции, которая выполняет запрос к базе данных
const query = async (queryString, value = []) =>
    await connectionsPool.query(queryString, Object.values(value));

exports.query = query;